import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import {Session } from '../models/session';
import {LocalStorageService} from '../services/local-storage.service';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate{

  constructor(private router: Router,private localStorage:LocalStorageService)
  {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean  {
    let session = this.localStorage.getSession();
    if (session != undefined)
        return true;

        //this.router.navigate(["/"]).then(result=>{window.location.href = 'http://localhost:4200/main-view';});
        this.router.navigate(["/main-view"]);
    return false;
  }

  
}
