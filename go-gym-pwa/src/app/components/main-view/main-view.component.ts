import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Session } from '../../models/session';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { CodeVerificationService } from '../../services/code-verification.service';
import { LocalStorageService } from '../../services/local-storage.service';
import { DecodedTokenService } from '../../services/decoded-token.service';
@Component({
  selector: 'app-main-view',
  templateUrl: './main-view.component.html',
  styleUrls: ['./main-view.component.css']
})
export class MainViewComponent implements OnInit {

  errors = false;
  smsError = false;
  // Alonso esta es la variable booleana para que haga la validacion si es gym o user
  isGym = false;

  modalFormLoginEmail = new FormControl('',   [Validators.required,
                                               Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]
                                               );
  modalFormLoginPassword = new FormControl('' ,  [Validators.required,
                                                  Validators.minLength(3)]
                                                  );

  modalFormLoginCode = new FormControl('' ,  [Validators.required,
                                                  Validators.minLength(3)]
                                                  );

  public session: Session;
  public verifyDataComplete: string;
  public code: string;
  constructor(private authService: AuthService, private router: Router, private decodedService: DecodedTokenService,
    private storageService: LocalStorageService, private codeVerification: CodeVerificationService) { }

  ngOnInit() {
  }

  login() {
    // tslint:disable-next-line:variable-name
    const value_email = this.modalFormLoginEmail.value;
    // tslint:disable-next-line:variable-name
    const value_password = this.modalFormLoginPassword.value;

    // tslint:disable-next-line:variable-name
    const status_email = this.modalFormLoginEmail.status;
    // tslint:disable-next-line:variable-name
    const status_password = this.modalFormLoginPassword.status;

    if (value_email !== '' && value_password !== '' && status_email !== 'INVALID' && status_password !== 'INVALID') {
      this.errors = false;

// Aqui va el metodo de buscar al usuario y ver si funka
// Este se asegura de que los input tengan contenido y que el correo sea un correo jajajaja.
      this.authService.login(value_email, value_password, this.isGym).toPromise()
      .then((res) => {
        this.session = new Session(res);
        //this.sendCodeToUser(this.session);  commented until the message service is paid
        
        //it works this way until the message service is paid
        const isGym = this.decodedService.getDecodedIsGym(this.session.access_token);
        this.storageService.saveSession(this.session);
        this.redirectAccordingTypeAccount(this.decodedService.getDecodedIsGym(this.session.access_token));
      })
      .catch(err => {
        this.errors = true;
        console.log(JSON.stringify(err));
      });
    } else {
      this.errors = true;
// Este es el primer error de validaciones, el ve si el correo es invalido y si la clave tambien
    }
  }

  sendCodeToUser(session: Session) {
    const id = this.decodedService.getDecodedIdGym(session.access_token);
    const isGym = this.decodedService.getDecodedIsGym(session.access_token);
    this.codeVerification.createCode(id, session.access_token).toPromise()
    .then(res => {
      console.log(res, 'res');
      return res;
    })
    .catch(err => {
      // tslint:disable-next-line:no-construct
      const x = new String(err['error']['text']);
      console.log(JSON.parse(x.substring(3, x.length)));
      const result = JSON.parse(x.substring(3, x.length));
      // tslint:disable-next-line:no-string-literal
      this.code = result['smsCode'];
      // console.log(result["smsCode"])
      // this.verifyCode();
    });

  }

  verifyCode() {
    // tslint:disable-next-line:variable-name
    const value_code = this.modalFormLoginCode.value;
    if (value_code === this.code) {
      this.storageService.saveSession(this.session);
      let isGym = this.decodedService.getDecodedIsGym(this.session.access_token)
      this.redirectAccordingTypeAccount(this.decodedService.getDecodedIsGym(this.session.access_token));
    } else {
      this.smsError = true;
    }
    console.log(this.code);
  }

  redirectAccordingTypeAccount(isGym: boolean) {
    if (isGym === false) {
      this.router.navigate( ['/home']);
      return;
    }
  }

}
