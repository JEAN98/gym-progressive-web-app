import { Component, OnInit } from '@angular/core';

declare let $: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    const boton = $('#btn-menu');
    // tslint:disable-next-line:variable-name
    const fondo_enlace = $('#fondo-enlace');

    // tslint:disable-next-line:only-arrow-functions
    boton.on('click', function(e) {
      fondo_enlace.toggleClass('active');
      $('#barra-lateral-izquierda').toggleClass('active');
      e.preventDefault();
    });

    // tslint:disable-next-line:only-arrow-functions
    fondo_enlace.on('click', function(e) {
      fondo_enlace.toggleClass('active');
      $('#barra-lateral-izquierda').toggleClass('active');
      e.preventDefault();
    });
  }

}
