import { Component, OnInit } from '@angular/core';
import {History} from '../../models/history';
import { HistoryService } from 'src/app/services/history.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {

  public histories : History[];
  public currentHistory: string;

  // tslint:disable-next-line:no-inferrable-types
  public chartType: string = 'line';

  public chartDatasets: Array<any> = [
    { data: [], label: 'Musculo' }
  ];

  // public chartDatasets: Array<any> = [
  //   { data: [65, 59, 80], label: 'My First dataset' }
  // ];

  public chartLabels: Array<any> = [];
  // public chartLabels: Array<any> = ['January', 'February', 'March'];


  public chartColors: Array<any> = [
    {
      backgroundColor: 'rgba(0, 137, 132, .2)',
      borderColor: '#f53756',
      borderWidth: 2,
    }
  ];

  public chartOptions: any = {
    responsive: true
  };

  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }

  // tslint:disable-next-line:variable-name
  constructor(private historyService: HistoryService, private localStorageServ:LocalStorageService) {
    this.currentHistory = 'height';
   }

  ngOnInit() {
    this.getHistories();
  }

  private getHistories()
  {
    this.historyService.getHistoriesByUser().subscribe((res:History[]) => {
      this.histories = res;
      this.localStorageServ.saveHistories(this.histories);
      this.buildChart(this.currentHistory);
    });
  }

  getCurrentHistory(historyName) {
    this.currentHistory =  historyName;
    this.buildChart(this.currentHistory);
  }

  buildChart(historyName) {

    const data = [];

    const label = [];

    // tslint:disable-next-line:prefer-for-of
    for (let index = 0; index < this.histories.length; index++) {
        // console.log(this.histories[index]);
        // tslint:disable-next-line:no-string-literal
        label.push(this.histories[index]['date'].toString());
        // tslint:disable-next-line:no-string-literal
        data.push(this.histories[index][historyName]);
    }

    this.chartDatasets[0].data = data;
    this.chartDatasets[0].label = historyName;
    this.chartLabels = label;
  }

}
