import { Component, OnInit } from '@angular/core';
import {UserService}         from '../../services/user.service';
import {User}                from '../../models/user';
import {Rol}                  from '../../models/rol';
import { ConnectionService } from 'ng-connection-service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public user     : User;
  public password : String;
  public rol      : Rol;
  public message  : String;
  public alertCss : String; 
  status = 'ONLINE';
  isConnected = true;


  constructor(private userService:UserService,private connectionService: ConnectionService){
    this.user = new User();
    this.rol =  new Rol();
    this.password = "";
    this.message = "";
    this.alertCss ="invisible";


    this.connectionService.monitor().subscribe(isConnected => {
          this.isConnected = isConnected;
          if (this.isConnected) {
            this.status = "ONLINE";
          }
          else {
            this.status = "OFFLINE";
          }
        console.log(status);
    })
  }

  ngOnInit() {
    this.getUserInformation();
  }

  private getUserInformation()
  {
    /*
    this.userService.getUserData().toPromise()
    .then((res:User) => {
      this.user = res;
      this.rol = this.user.rol;
      console.log(this.user);
    })
    .catch( err =>{
        console.log(err);
    });*/
    this.userService.getUserData().subscribe((res:User) => {
      this.user = res;
      console.log(JSON.stringify(res));
    })
  }

  public updateProfile()
  {
    if(!this.verifyInputs())
    {
      //console.log("Current User",this.user);
      let currentUser = this.user;
      this.userService.updateUserData(currentUser).toPromise()
        .then((res) => {
          //this.user = res;
        })
        .catch( err =>{
            console.log(err);
        });
        this.message = "¡Los cambios se han guardado con éxito!";
        this.alertCss = "alert alert-success";
    }
    else{
      this.message = "¡Debe completar todos los campos!";
      this.alertCss = "alert alert-danger";
    }
   
  }

  private verifyInputs():boolean
  {
    for (var k in this.user) {
      if (typeof this.user[k]  == "string" && this.user[k] == "") {
        return true;
      }
    }
    return false;
  }

}
