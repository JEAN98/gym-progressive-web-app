import { Component, OnInit }     from '@angular/core';
import {HistoryService}          from '../../services/history.service';
import {History}                 from '../../models/history';
import { Router,ActivatedRoute } from '@angular/router';
import { LocalStorageService } from '../../services/local-storage.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  public histories : History[];
  public idGym     : number;
  constructor(private historyService: HistoryService,private route: ActivatedRoute, 
    private localStorageServ:LocalStorageService){
    this.histories = [];
    this.idGym = parseInt(this.route.snapshot.paramMap.get('idGym'));
  }

  ngOnInit() {
    this.getHistories();
  }

  private getHistories()
  {
   
     this.historyService.getHistoriesByUser().subscribe((res:History[]) => {
      this.histories = res;
      this.localStorageServ.saveHistories(this.histories);
    })
  }

}
