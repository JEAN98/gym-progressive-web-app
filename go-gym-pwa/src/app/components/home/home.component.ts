import { Component, OnInit, ViewChild } from '@angular/core';
import {UserGymService}      from '../../services/user-gym.service';
import {UserGym}             from '../../models/usergym';
import {LocalStorageService} from '../../services/local-storage.service';
import { Gym }               from '../../models/gym';
import {ScheduleService}  from '../../services/schedule.service';
import { GymSchedule }       from 'src/app/models/gym-schedule';
import { Session } from '../../models/session';
import { NewsComponent } from '../news/news.component';
import { HeaderComponent } from '../header/header.component';
import { GymScheduleService } from '../../services/gym-schedule.service';
import { Schedule } from '../../models/schedule';
import { Address } from '../../models/Address';
import { DistrictService } from '../../services/district.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public currentGym:Gym;

  public schedules: Schedule[];
  public currentScheduleList: Schedule[];
  
  public address: Address;
  public daySelected = 0;
  public gymScheduleList: GymSchedule[];
  public dayList: any[];

  public session: Session
  @ViewChild(NewsComponent) newsComponent: NewsComponent;

  @ViewChild(HeaderComponent) headerComponent: HeaderComponent;

  constructor(private userGymService:UserGymService,private localStorageServ:LocalStorageService,
            private schedule:ScheduleService, private gymScheduleService: GymScheduleService,
            private districtService: DistrictService,)
  {
    this.currentGym = new Gym();
    this.schedules = [];
    this.dayList = [];
    this.currentScheduleList = [];
    
  } 
  ngOnInit() {
    this.session = this.localStorageServ.getSession();
    this.getGymData();
  }

  // this method get address of the gym
  getLocation(id: number) {
    this.districtService.getLocation(id, this.session.access_token).toPromise()
    .then((res) => {
      this.address = res[0];
    })
    .catch(err => {
      console.log(JSON.stringify(err));
    });
  }

  private getGymData()
  {
    let gym = this.localStorageServ.getCurrentGym();
    if( gym == undefined)
    {
      this.userGymService.getGymByUserId(this.session).toPromise()
        .then((res:UserGym) => {
          this.currentGym = res[0].gym;
          this.localStorageServ.saveCurrentGym(this.currentGym);
          //console.log(this.currentGym, "CurrentGym");
          this.loadMainData();
        })
        .catch( err =>{
            console.log(err);
        });
    }
    else{
      this.currentGym = gym;
      this.loadMainData();
      //console.log(this.currentGym, "CurrentGym");
    }
  }

  private loadMainData(){
    this.getGymSchedule();
    this.getLocation(this.currentGym.idDistrict);
    this. getSchedules();
    this.newsComponent.getNewsByCurrentGym(this.currentGym.id, this.session.access_token, this.currentGym.name);
    this.headerComponent.getGymName();
  }


  // This method get DAYS
  private getGymSchedule()
  {
    this.gymScheduleService.getAllGymSchedule(this.session.access_token).toPromise()
      .then((res) => {
        this.gymScheduleList = res;
        //console.log(res);
      })
      .catch(err => {
        console.log(JSON.stringify(err));
      });
  }


  // This method get the SCHEDULE BY GYM
  getSchedules() {
    this.schedule.getSchedule(this.currentGym.id, this.session.access_token).toPromise()
    .then((res:Schedule[]) => {
      this.schedules = res;
      this.currentScheduleList = this.schedules;
      this.buildSchedulesList();
      //console.log(this.schedules);
    })
    .catch( err =>{
        console.log(err);
    });
  }

   // This method build the schedule list for the table
   buildSchedulesList() {
    this.dayList = [];
    this.dayList.push(new GymSchedule(0, 'Todos'));

    // tslint:disable-next-line:prefer-for-of
    for (let index = 0; index < this.schedules.length; index++) {
      // tslint:disable-next-line:no-string-literal
      if (!this.verifyDay(this.schedules[index]['idGymSchedule']['id'])) {
              // tslint:disable-next-line:no-string-literal
            this.dayList.push(this.schedules[index]['idGymSchedule']);
          }
    }
  }

  // This method verify the day that the table has selected
  private verifyDay(currentDay) {
    // tslint:disable-next-line:prefer-for-of
    for (let index = 0; index < this.dayList.length; index++) {
      // tslint:disable-next-line:no-string-literal
      if (this.dayList[index]['id'] === Number(currentDay)) {
        return true;
      }
    }
    return false;
  }

  // This method get the current day that the table selected
  getCurrentDay(day) {
    const currentDay =  Number(day);
    if (currentDay === 0 ) {
      this.currentScheduleList = this.schedules;
    } else {
      this.currentScheduleList =  this.schedules.filter(
        // tslint:disable-next-line:no-string-literal
        schedule => schedule.idGymSchedule['id'] === currentDay);
    }
  }

}
