import { Component, OnInit, Input } from '@angular/core';
import {NewsService} from '../../services/news.service';
import {LocalStorageService} from '../../services/local-storage.service';
import {News} from '../../models/news';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  public news: News[];
  public gymName: string;
  constructor(private newsService: NewsService, private localStorageSrc: LocalStorageService) {
              this.news = [];
  }

  ngOnInit() {
    // this.getNewsByCurrentGym();
  }

  public getNewsByCurrentGym(idGym: number, token: string, gymName: string) {
    this.gymName = gymName;
    // let idGym = this.localStorageSrc.getCurrentGym().id;
    this.newsService.getNews(idGym, token).toPromise()
    .then((res: News[]) => {
      this.news = res;
      // console.log(this.news,"news test");
    })
    .catch( err => {
        console.log(err);
    });
  }

}
