import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from '../../services/local-storage.service';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

// declare var jQuery: any;
declare let $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public gymName: string;

  constructor(private localStorageServ: LocalStorageService,private authService:AuthService, private router:Router) {
    this.gymName = "";
  }

  ngOnInit() {
      // tslint:disable-next-line:only-arrow-functions
      $('#menu-navegacion .navbar-toggler').click(function() {
        $('.boton-menu').toggleClass('icono-cerrar');
      });

      // tslint:disable-next-line:only-arrow-functions
      $('#menu-navegacion .navbar-nav a').click(function() {
        $('.boton-menu').removeClass('icono-cerrar');
        $('#menu-navegacion .navbar-collapse').collapse('hide');
    });

      this.navbar();
      this.getGymName();
  }

  navbar() {

    // tslint:disable-next-line:only-arrow-functions
    window.onscroll = function(e) {
      const scroll = window.scrollY;
      const barraAltura = $('.navbar').innerHeight();

      const headerScroll = document.querySelector('#menu-navegacion');
      if (scroll > 85) {
        $('.navbar').addClass('fixed');
        $('body').css({'margin-top': barraAltura + 'px'});
        headerScroll.classList.add('bg-success');
      } else {
        $('.navbar').removeClass('fixed');
        $('body').css({'margin-top': '0px'});
        headerScroll.classList.remove('bg-success');      }
  };
  }

  getGymName() {
    let gym = this.localStorageServ.getCurrentGym();
    if(gym)
    {
      this.gymName = gym.name;
    }
    else
    {
      this.gymName = "";
    }
  }

  logout()
  {
    this.authService.logout().toPromise()
    .then((res) => {
      //console.log(res);
      this.localStorageServ.deleteData();
      this.router.navigate(['/main-view']);
    })
    .catch( err =>{
        console.log(err);
    });
  }

}
