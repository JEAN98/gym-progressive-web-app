export class Message {
    id: number;
    isGym: string;
    constructor(id, isGym){
        this.id = id;
        this.isGym = isGym;
    }
}
