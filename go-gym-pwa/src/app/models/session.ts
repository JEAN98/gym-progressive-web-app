import { JwtHelperService } from '@auth0/angular-jwt';

export class Session {
    // tslint:disable-next-line:variable-name
    access_token: string;
    // tslint:disable-next-line:variable-name
    token_type: string;
    // tslint:disable-next-line:variable-name
    expires_in: number;
    jwtService: JwtHelperService;
    constructor(sessionJson) {
        // tslint:disable-next-line:no-string-literal
        this.access_token = sessionJson['access_token'];
        // tslint:disable-next-line:no-string-literal
        this.token_type  = sessionJson['token_type'];
        // tslint:disable-next-line:no-string-literal
        this.expires_in = sessionJson['expires_in'];
        this.jwtService = new JwtHelperService();
    }

    public getDecodedToken(): any {
       return this.jwtService.decodeToken(this.access_token);
    }

    public isTokenExpired(): boolean {
      return this.jwtService.isTokenExpired(this.access_token);
    }
}
