// import { Rol } from './Rol';

export class User {
    id: number;
    name: string;
    lastName: string;
    phone: string;
    born: string;
    gender: string;
    idNumber: string;
    email: string;
    exercise: number;
    liqueur:number;
    cigar:number;
    level:string;
    // tslint:disable-next-line:variable-name
    email_verified_at?: any;
    // tslint:disable-next-line:variable-name
    created_at: string;
    // tslint:disable-next-line:variable-name
    updated_at: string;
}

