import {Gym} from './gym';
export class UserGym {
    id: number;
    status: number;
    // tslint:disable-next-line:variable-name
    created_at: string;
    // tslint:disable-next-line:variable-name
    updated_at: string;
    idGym: number;
    idUser: number;
    gym: Gym;
}
