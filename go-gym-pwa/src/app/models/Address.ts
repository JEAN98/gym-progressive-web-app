export class Address {
    id: number;
    districtName: string;
    cantonName: string;
    provinceName: string;
}