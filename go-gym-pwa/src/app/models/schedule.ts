export class Schedule {
    id: number;
    startTime: string;
    startMinutes: string;
    startPeriod: string;
    endTime: string;
    endMinutes: string;
    endPeriod: string;
    created_at: string;
    updated_at: string;
    idGymSchedule: number;
    idGym: number;
}
