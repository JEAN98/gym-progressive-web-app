export class Gym {
    id: number;
    name: string;
    foundationYear: string;
    gymPhone: string;
    status: number;
    email: string;
    created_at?: any;
    updated_at?: any;
    typeGym: number;
    idDistrict: number;
}
