import { Component }         from '@angular/core';
import {SwUpdate }           from '@angular/service-worker';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'go-gym-pwa';

  constructor(updates:SwUpdate)
  {
    //verify new changes 
    updates.available.subscribe(event => {
      // this.update = true;
      updates.activateUpdate().then(() => document.location.reload());
     });


     
  }
}
