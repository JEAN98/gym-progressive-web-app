import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProfileComponent}       from './components/profile/profile.component';
import {HomeComponent}          from './components/home/home.component';
import {HistoryComponent}       from './components/history/history.component';
import { DashboardComponent }   from './components/dashboard/dashboard.component';
import { RoutinesComponent }    from './components/routines/routines.component';
import {NotFoundComponent}      from './components/not-found/not-found.component';
import {AuthGuard}              from './guards/auth.guard';
import {MainViewComponent}      from './components/main-view/main-view.component';
import { ChartComponent } from './components/chart/chart.component';

const routes: Routes = [
  { path: 'profile', component: ProfileComponent,canActivate: [AuthGuard] },
  { path: 'home', component: HomeComponent,canActivate: [AuthGuard] },
  { path: 'dashboard', component: DashboardComponent,canActivate: [AuthGuard] },
  { path: 'routines', component: RoutinesComponent ,canActivate: [AuthGuard]},
  { path: 'histories', component: HistoryComponent ,canActivate: [AuthGuard]},
  { path: 'chart', component: ChartComponent ,canActivate: [AuthGuard]},
  {path: 'not-found', component: NotFoundComponent},
  {path: 'main-view' , component: MainViewComponent},
 // { path: 'history/:idGym', component: HistoryComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'not-found' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
