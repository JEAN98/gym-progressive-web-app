import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import {SessionService} from './session.service';
import {Session} from '../models/session';
@Injectable({
  providedIn: 'root'
})
export class HeaderService {
  public urlBase: string;
  public guestURL: string;
  public session: Session;
  constructor(private sessionService: SessionService) {
    this.urlBase  = 'http://127.0.0.1:8000/api/gymMember';
    this.guestURL = 'http://127.0.0.1:8000/api/guest'; 
  }

  loadSession():Session{
    return this.sessionService.getCurrenSession();
  }

  getGuestURL(){
    return 'http://localhost:8000/api';
  }

  getHeaderLogin() {
    // tslint:disable-next-line:new-parens
    const header = new HttpHeaders;
    header.append('Content-Type', 'application/x-www-form-urlencoded');
    return {headers: header};
  }

  getHeaderToken() {

  /*  const header = new HttpHeaders({
      'Content-Type': 'application/json',
      // tslint:disable-next-line:object-literal-key-quotes
      'Authorization': 'Bearer ' + this.session.access_token
    }); */
    const header = new HttpHeaders({
      'Content-Type': 'application/json',
      // tslint:disable-next-line:object-literal-key-quotes
      'Authorization': 'Bearer ' + this.loadSession().access_token
    });
    return {headers: header};
  }

  getHeaderTokenByParams(token: string) {

    const header = new HttpHeaders({
      'Content-Type': 'application/json',
      // tslint:disable-next-line:object-literal-key-quotes
      'Authorization': 'Bearer ' + token
    });
    return {headers: header};
  }

  getHeaderCode() {
    const header = new HttpHeaders({
    // tslint:disable-next-line:object-literal-key-quotes
    'Accept': 'text/html, application/xhtml+xml, */*',
    'Content-Type': 'application/x-www-form-urlencoded',
      });
    return {headers: header};  }

}
