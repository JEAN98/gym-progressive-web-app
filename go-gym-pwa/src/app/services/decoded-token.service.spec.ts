import { TestBed } from '@angular/core/testing';

import { DecodedTokenService } from './decoded-token.service';

describe('DecodedTokenService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DecodedTokenService = TestBed.get(DecodedTokenService);
    expect(service).toBeTruthy();
  });
});
