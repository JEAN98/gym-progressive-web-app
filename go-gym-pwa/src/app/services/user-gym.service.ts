import { Injectable }        from '@angular/core';
import { HttpClient, HttpHeaders }        from '@angular/common/http';
import { HeaderService }     from './header.service';
import {UserGym}             from '../models/usergym';
import {LocalStorageService} from '../services/local-storage.service';
import { Session } from '../models/session';


@Injectable({
  providedIn: 'root'
})
export class UserGymService {

  public userGymURL :String;

  constructor(private http: HttpClient, private headerService: HeaderService
              ,private localStorageSrv:LocalStorageService)
  {
    this.userGymURL = this.headerService.urlBase + "/userGym/";
  }

  public getGymByUser()
  {
    let url = this.userGymURL + this.headerService.session.getDecodedToken()['sub'];
    return this.http.get(url, this.headerService.getHeaderToken());
  }

  public getGymByUserId(session: Session)
  {
    let url = this.userGymURL + session.getDecodedToken()['sub'];
    return this.http.get(url, this.headerService.getHeaderTokenByParams(session.access_token));
  }

  

}
