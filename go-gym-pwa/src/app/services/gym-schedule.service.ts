import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HeaderService } from './header.service';

@Injectable({
  providedIn: 'root'
})
export class GymScheduleService {

  private gymScheduleURL: string;
  constructor(private http: HttpClient, private headerService: HeaderService) { 
    this.gymScheduleURL = this.headerService.urlBase + '/gymSchedule';
  }
  refreshUrl(){
    this.gymScheduleURL = this.headerService.urlBase + '/gymSchedule';
  }
  getAllGymSchedule(token: string):any{
    return this.http.get(this.gymScheduleURL, this.headerService.getHeaderTokenByParams(token));
  }
}
