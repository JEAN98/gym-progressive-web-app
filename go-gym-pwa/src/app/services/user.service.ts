import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import {HeaderService} from './header.service';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userURL: string;
  constructor(private http: HttpClient, private headerService: HeaderService) {
    this.userURL = this.headerService.urlBase + '/user';
  }

  public getUserData() {
    var url = this.userURL + '/' + this.headerService.loadSession().getDecodedToken()['sub'];
    return this.http.get(url, this.headerService.getHeaderToken());
  }

  public updateUserData(user:User)
  {
    var url = this.userURL + '/' + this.headerService.loadSession().getDecodedToken()['sub'];
    return this.http.put(url,JSON.stringify(user),this.headerService.getHeaderToken());
  }
}
