import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HeaderService } from './header.service';

@Injectable({
  providedIn: 'root'
})
export class DistrictService {

  private districtURL: string;
  constructor(private http: HttpClient, private headerService: HeaderService) {
    this.districtURL = this.headerService.urlBase + '/district';
  }

  getLocation(idDistrict: number, token: string) {
    let url = this.districtURL += '/location/' + idDistrict;
    return this.http.get(url, this.headerService.getHeaderTokenByParams(token));
  }
}
