import { Injectable } from '@angular/core';
import { HeaderService } from './header.service';
import { News } from '../models/news';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class NewsService {
  private newsURL:string;

  constructor(private http: HttpClient, private headerService: HeaderService)
  {
    this.newsURL = this.headerService.urlBase + "/news/gym/";
  }

  getNews(idGym: number, token: string): any {
     let url = this.newsURL + idGym.toString();
     return this.http.get(url, this.headerService.getHeaderTokenByParams(token));
  }
}
