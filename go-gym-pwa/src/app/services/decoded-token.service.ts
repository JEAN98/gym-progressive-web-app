import { Injectable } from '@angular/core';
import * as jwt_decode from 'jwt-decode';
@Injectable({
  providedIn: 'root'
})
export class DecodedTokenService {

  constructor() { }

  decodeToken(token: string) {
    try {
        return jwt_decode(token);
    } catch (Error) {
        return null;
    }
}

getDecodedIdRol(token: string): any {
    try {
        return jwt_decode(token).rolID;
    } catch (Error) {
        return null;
    }
}

getDecodedIsGym(token: string): any {
    try {
        return jwt_decode(token).isGym;
    } catch (Error) {
        return null;
    }
}

  getDecodedIdGym(token: string): any {
    try {
        return jwt_decode(token).sub;
    } catch (Error) {
        return null;
    }
  }
}
