import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import {HeaderService} from './header.service';

@Injectable({
  providedIn: 'root'
})
export class HistoryService {

  private historyURL: string;
  constructor(private http: HttpClient, private headerService: HeaderService) {
     this.historyURL = this.headerService.urlBase + '/history';
  }

  public getHistoriesByUser() {
 
    let url =  this.historyURL +'/userHistory/' + this.headerService.loadSession().getDecodedToken()['sub'];
    return this.http.get(url, this.headerService.getHeaderToken());
  }

  public getHistoriesByGym(idGym: number) {
   
    let userParam = 'idUser=' + this.headerService.session.getDecodedToken()['sub'];
    let gymParam = 'idGym=' + idGym;
    let url =  this.historyURL + '?' + userParam + '&'+ gymParam;
    return this.http.get(url, this.headerService.getHeaderToken());
  }
}
