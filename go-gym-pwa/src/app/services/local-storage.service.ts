import { Injectable } from '@angular/core';
import {Session} from '../models/session';
import { Gym } from '../models/gym';
import {History} from '../models/history';
@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor(private localStorageSrv: LocalStorageService) {
    this.saveToken();
    this.getSession();
  }

  getSession(){
    if(localStorage.getItem('session')!= undefined)
    {
      return new Session(JSON.parse(localStorage.getItem('session')));
    }
    return undefined;
  }

  
  saveSession(session: Session) {
    localStorage.setItem( 'session', JSON.stringify( session ));
  }

  saveHistories(histories: History[]) {
    localStorage.setItem( 'histories', JSON.stringify( histories ));
  }
  getHistories(){
    if(localStorage.getItem('histories')!= undefined)
    {
      return JSON.parse(localStorage.getItem('histories'));
    }
    return undefined;
  }

  saveCurrentGym(gym:Gym)
  {
    localStorage.setItem('currentGym',JSON.stringify(gym));
  }

  getCurrentGym()
  {
    let currentGym = localStorage.getItem('currentGym'); 
    if(currentGym != null && currentGym != "undefined")
    {
      return JSON.parse(localStorage.getItem('currentGym')) as Gym;
    }
    return undefined;
  }

  // This is a simple test
  public saveToken() {
    /* const session = {
      "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODAwMFwvYXBpXC9ndWVzdFwvdXNlcnNcL3Nlc3Npb24iLCJpYXQiOjE1NTU5MDIwMDYsImV4cCI6MTU1NTkzMDgwNiwibmJmIjoxNTU1OTAyMDA2LCJqdGkiOiJ1UlhGdlhCR0ZqZW12UVVSIiwic3ViIjoxLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIiwiaXNHeW0iOmZhbHNlfQ.2SQE17qfvNoGFIWUnJZ72-xT9yl0Hc87omfFX8-PoLY",
      "token_type": "bearer",
      "expires_in": 230400
      };
    localStorage.setItem('session', JSON.stringify(session)); */
  }


  public deleteData()
  {
    localStorage.clear();
  }

}
