import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {LocalStorageService} from './local-storage.service';
// import {Session}             from '../models/session';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  // public session:Session;
  public session: any;


  constructor(private http: HttpClient, private localStorageSrv: LocalStorageService) {
    this.session = this.localStorageSrv.getSession();
    this.printTokenData();
  }

  private printTokenData() {
    /* console.log('isTokenExpired = ', this.session.isTokenExpired());
    console.log(this.session);
    console.log('Payload = ', this.session.getDecodedToken()); */
  }

  public getCurrenSession():any
  {
    return this.localStorageSrv.getSession();
  }

 /*  logout() {
    
    this.authService.logout().toPromise()
    .then((res) => {
      this.deleteData();
      this.router.navigate( ['/mainView']);
    })
    .catch(err => {
      console.log(err);
    });

  } */

}
