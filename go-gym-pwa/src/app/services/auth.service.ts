import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { HeaderService } from './header.service';
// import { Session } from '../models/Session';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private sessionURL: string; // base url
  
  constructor(private http: HttpClient, private headerService: HeaderService) {
    this.sessionURL = this.headerService.getGuestURL() + '/guest/users/session';
  }
  /* refreshUrl() {
    this.loginUrl = this.headerService.getGuestURL() + '/guest/users/session';
  } */
  login(email: string, password: string, isGym: boolean) {
    //this.refreshUrl();
    const body = new HttpParams()
    .set('email', email)
    .set('password', password)
    .set('isGym', isGym ? "gym" : "member");
    return this.http.post(this.sessionURL, body, this.headerService.getHeaderLogin());
  }

  logout() {
    return this.http.delete(this.sessionURL, this.headerService.getHeaderToken());
  }

}
