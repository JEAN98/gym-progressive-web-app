import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders }        from '@angular/common/http';
import { HeaderService }     from './header.service';

@Injectable({
  providedIn: 'root'
})
export class CodeVerificationService {

  private urlCode: string;
  constructor(private http: HttpClient, private headerService: HeaderService) { 
    this.urlCode = this.headerService.getGuestURL() + '/guest/message';
  }

  createCode(id: number, token: string):any{
    
    let url = this.urlCode + '/' + id.toString() + '/' + 'member';
    let header = this.headerService.getHeaderCode();
    return this.http.post(url, header);
  }

}
