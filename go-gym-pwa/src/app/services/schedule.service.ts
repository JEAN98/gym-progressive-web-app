import { Injectable } from '@angular/core';
import { HeaderService } from './header.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {


  private scheduleURL: string;
  constructor(private http: HttpClient, private headerService: HeaderService) { 
    this.scheduleURL = this.headerService.urlBase + '/schedule/gym';
  }
 
  getSchedule(idGym: number, token: string):any{
    let url = this.scheduleURL + '/' + idGym;
    return this.http.get(url, this.headerService.getHeaderTokenByParams(token));
  }
}
